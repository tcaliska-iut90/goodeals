# GOODEALS
## Lien du Gitlab :

[Gitlab](https://gitlab.com/tcaliska-iut90/goodeals)

## Lien du Site :

[GOODEALS](https://goodeals-tcaliska-iut90-baa3b41b97d201d6c5c3fbf45076671a194c4c2.gitlab.io/)

## Membres du groupe :

- [CALISKAN Turker](mailto:turker.caliskan@edu.univ-fcomte.fr)  
- [CARUHEL Rémy](mailto:remy.caruhel@edu.univ-fcomte.fr)   
- [LANBLOT Manon](mailto:manon.lanblot@edu.univ-fcomte.fr)
- [LENBLE Hugo](mailto:hugo.lenble@edu.univ-fcomte.fr)

<br>

## Présentation 

Site d'ecommerce avec plusieurs pages.

## Répartition du travail

### Développement site

- CARUHEL Rémy
  - Page produit
  - Footer
- LEMBLE Hugo
  - Page panier
- LAMBLOT Manon
  - Page boutique
- CALISKAN Turker
  - Page d’accueil
  - Menu de naviguation
  - Gitlab

  ### Site

Pour acceder à la page du panier, il faut cliquer dessus.(on sait jamais)
Pour la resposivité de la page d'accueil, les images sont responsive tout en restant aligné(je me suis rendu compte trop tard de mon erreur)
